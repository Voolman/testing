import 'package:flutter/material.dart';

class TestPage extends StatefulWidget {
  const TestPage({super.key});

  @override
  State<TestPage> createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {

  int num = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        height: double.infinity,
        width:  double.infinity,
        child: Expanded(
          child: FilledButton(
              onPressed: (){
                setState(() {
                  num += 1;
                });
              },
              child: Text(
                num.toString(),
              style: TextStyle(
                fontSize: num.toDouble()
              ))
          ),
        ),
      ),
    );
  }
}